<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Users_model extends CI_Model {

	/**
	 * Constructor
	 */
	function __construct()
	{
		parent::__construct();
	}

	/**
	 * Add a new user
	 *
	 * @param  array $data
	 * @return mixed|boolean
	 */
	public function insertUser($data){
		$query = $this->db->select('*')->from('users')->where('email', $data['email'])->get();

		if($query->num_rows() == 0){
			$this->db->insert('users', $data);
			$this->session->tamp = 'success_reg';
		}else{
			$this->session->tamp = 'fail_reg';
		}
	}

	/**
	 * Get list of users
	 *
	 * @return array|boolean
	 */
	public function getAllUser(){
		$q = $this->db->select('*')->from('users')->get();
		return $q->result();
	}

	/**
	 * Get user id
	 *
	 * @param  int $id
	 * @return array|boolean
	 */
	public function getId(){
		$q = $this->db->select('id')->from('users')->where('email', $this->session->userdata('email'))->get();
		return $q->row()->id;
	}

	/**
	 * Check for valid login credentials
	 *
	 * @param  string $email
	 * @param  string $password
	 * @return array|boolean
	 */
	function login($email=NULL, $password=NULL)
	{
		if ($email && $password)
		{
			$sql = "
				SELECT
					id,
					username,
					password,
					first_name,
					last_name,
					email,
					is_admin,
					status,
					created_at,
					updated_at
				FROM users
				WHERE email = " . $this->db->escape($email) . "
					AND status = '1'
					AND deleted = '0'
				LIMIT 1
			";

			$query = $this->db->query($sql);

			if ($query->num_rows())
			{
				$results = $query->row_array();

				if ($results['password'] == $password)
				{
					unset($results['password']);

					return $results;
				}
			}
		}

		return FALSE;
	}
}
