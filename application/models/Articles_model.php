<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Articles_model extends CI_Model {

	/**
	 * Constructor
	 */
	function __construct()
	{
		parent::__construct();
	}

	/**
	 * Add a new article
	 *
	 * @param  array $data
	 * @return mixed|boolean
	 */
	public function insertArticle($data){
		$last_row_id =$this->db->select('id')->order_by('author_id',"desc")->limit(1)->get('post')->row();
		$this->db->insert('articles', $data);
	}

	/**
	 * Get list of articles
	 *
	 * @return array|boolean
	 */
	public function getAllArticle(){
		$q = $this->db->select('*')->from('articles')->get();
		return $q->result();
	}

	/**
	 * Get specific article
	 *
	 * @param  int $id
	 * @return array|boolean
	 */
	public function getArticleByID($id){
		$data =$this->db->select('*')->from('articles')->where('id',$id)->get();
		return $data->result();


		return $data->result();
	}

	public function upload($data)
	{
		$this->db->insert('articles', $data);
	}

	public function search($title){
		return $this->db->query("SELECT * from articles where title like '%$title%'");
	}

}
