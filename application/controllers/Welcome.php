<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct(){
		parent::__construct();
	}

	public function index_new()
	{
		$this->session->tamp2 = 'home';
		$this->session->tamp = 'home';
		$this->load->view('header');
		$this->load->view('home');
		$this->load->view('footer');
	}

	public function index(){
		if($this->session->tamp == 'success_login'){
			$this->session->tamp2 = 'home';
			$this->index_next();
		}else{
			$this->index_new();
		}
	}

	public function index_next(){
		$this->session->tamp2 = 'home';
		$this->load->view('header');
		$this->load->view('home');
		$this->load->view('footer');
	}

	public function admin(){
		$this->load->model('Users_model');
		$this->session->tamp2 = 'account';

		$data['user_list'] = $this->Users_model->getAllUser();
		$this->load->view('header');
		$this->load->view('v_admin', $data);
		$this->load->view('footer');
	}
}
