<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Journal extends CI_Controller {

	/**
	 * Constructor
	 */
	function __construct()
	{
		parent::__construct();

		// load the articles model
		$this->load->model('articles_model');
	}


	/**
	 * Default
	 */
	public function index() {

		$data['listjurnal'] = $this->articles_model->getAllArticle();

		// load view
		$this->load->view('header');
		$this->load->view('jurnalview', $data);
		$this->load->view('footer');
	}

	public function upload()
	{
		$this->load->view('header');
		$this->load->view('upload');
		$this->load->view('footer');
	}

	public function do_upload()
	{
		$config['upload_path']          = './assets/uploads/';
		$config['allowed_types']        = 'pdf';

		$this->load->library('upload', $config);


			$data = array('upload_data' => $this->upload->data());

			foreach ($data as $item){
				$file_url = $item['full_path'];
			}

			$data = array(
				'id' => 'null',
				'title' => $this->input->post('title'),
				'author' => $this->input->post('author'),
				'year' => $this->input->post('year'),
				'doi' => $this->input->post('doi'),
				'file_url' => $file_url
			);

			$data['data'] = $this->articles_model->upload();

			$this->load->view('header');
			$this->load->view('upload');
			$this->load->view('footer');

	}

	public function search(){
		$q = $this->input->post('cari');
		$this->load->model('Articles_model');
		$data['listjurnal'] = $this->Articles_model->search($q)->result();

		$this->load->view('header');
		$this->load->view('jurnalview', $data);
		$this->load->view('footer');
	}

	public function upload_jurnal()
	{
		$data['status'] = "hubla";
		//hapus if dibawah jika sudah bisa login.
		if (isset($_POST['submit'])) {
			# code...
		}else{
			$this->session->sess_destroy();
		}

		if($this->session->id_user == null){

			$arraydata = array(
                'id_user'  => 3,
                'nama_penulis' => 'Budikandri Suhartoni',
                'logged_in' => TRUE
			);

			$this->session->set_userdata($arraydata);
			$this->load->view('header');
			$this->load->view('v_upload_file',$data);
			$this->load->view('footer');
			// sampai loginnya bener ini di komen
			// $this->load->view('header');
			// $this->load->view('v_Upload_Oleh_Asing');
			// $this->load->view('footer');
		}else{
			if (isset($_POST['submit'])) {
				$config['upload_path'] = './journal/';
				$config['allowed_types'] = 'pdf';

				if ( ! $this->upload->do_upload('upload'))
                {
                        $error = array('error' => $this->upload->display_errors());
						$this->load->view('header');
						$this->load->view('v_upload_file', $error);
						$this->load->view('footer');
                }
                else
                {
                        $data = array('upload_data' => $this->upload->data());
						$this->load->view('header');
						$this->load->view('v_upload_success');
						$this->load->view('footer');
                }
			}

		}
	}

	public function lihat_jurnal($id)
	{
		$data['jurnal'] = $this->articles_model->getArticleById($id);

		$this->load->view('header');
		$this->load->view('jurnaldetail',$data);
		$this->load->view('footer');
	}

	public function ambil_jurnal($id)
	{
		$arg['id'] = $id;
		$arg['data'] = $this->articles_model->getArticleByID($id);
		$this->load->view('header');
		$this->load->view('v_detail_jurnal',$arg);
		$this->load->view('footer');
	}

	public function download($id){
		$coba = $this->articles_model->getArticleByID($id);
		foreach ($coba as $key) {
			$down = $key->file_url;
		}

		force_download($down);
		redirect('Journal');
	}
}
