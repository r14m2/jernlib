<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	/**
	 * Constructor
	 */
	public function __construct()
	{
		parent::__construct();

		// load the users model
		$this->load->model('users_model');
	}


	/**
	 * Default
	 */
	public function index() {
		$this->load->view('header');
		$this->load->view('home');
		$this->load->view('footer');
	}


	/**
	 * Login Form
	 */
	function login()
	{
		// If logged in, redirect
		if ($this->session->userdata('logged_in'))
		{
			$logged_in_user = $this->session->userdata('logged_in');
			if ($logged_in_user['is_admin'])
			{
				redirect('Welcome/admin');
			}
			else
			{
				redirect(base_url());
			}
		}

		// set form validation rules
		$this->form_validation->set_error_delimiters($this->config->item('error_delimeter_left'), $this->config->item('error_delimeter_right'));
		$this->form_validation->set_rules('email', 'Email', 'required|trim|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'required|trim|max_length[72]|callback__check_login');

		// If login validation succeed, redirect
		if ($this->form_validation->run() == TRUE)
		{
			$logged_in_user = $this->session->userdata('logged_in');
			if ($logged_in_user['is_admin'])
			{
				// redirect to admin dashboard
				redirect('Welcome/admin');
			}
			else
			{
				// redirect to landing page
				redirect(base_url());
			}
		}

		// When not logged in or validation error, show login form
		$this->load->view('header');
		$this->load->view('login');
		$this->load->view('footer');
	}


	/**
	 * Logout
	 */
	function logout()
	{
        $this->session->unset_userdata('logged_in');
        $this->session->sess_destroy();
        redirect('login');
	}


	/**
	 * Registration Form
	 */
	public function signup()
	{
		$this->session->tamp2 = 'signup';
		$this->session->tamp = 'signup';
		$this->load->view('header');
		$this->load->view('signup');
		$this->load->view('footer');
	}

	public function prosesSignup()
	{
		$data['id'] = $this->input->post('id');
		$data['username'] = $this->input->post('username');
		$data['password'] = $this->input->post('password');
		$data['first_name'] = $this->input->post('first_name');
		$data['last_name'] = $this->input->post('last_name');
		$data['email'] = $this->input->post('email');
		$data['is_admin'] = '0';
		$data['status'] = '1';
		$data['deleted'] = '0';
		$data['created_at'] = date("Y-m-d H:i:s");
		$data['updated_at'] = date("Y-m-d H:i:s");
		$this->users_model->insertUser($data);

		$success['succ'] = "Register success";
		$this->index();

		/*$this->form_validation->set_rules('username', 'Username', 'required|valid_email|is_unique[user.email]', array('is_unique' => "Email Sudah Ada"));
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('passconf', 'Password Confirmation', 'matches[password]', array('matches' => "Password do not match"));*/

	}

	/**************************************************************************************
	 * PRIVATE VALIDATION CALLBACK FUNCTIONS
	 **************************************************************************************/

	/**
	 * Verify the login credentials
	 *
	 * @param  string $password
	 * @return boolean
	 */
	function _check_login($password)
	{
		$login = $this->users_model->login($this->input->post('email', TRUE), $password);

		if ($login)
		{
			$this->session->set_userdata('logged_in', $login);
			return TRUE;
		}

		return FALSE;
	}
}
