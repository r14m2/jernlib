	<div class="container p-5 m-auto col-md-5">
		<div class="row">
			<div class="col pt-4">
				<?php if (validation_errors()) : ?>
					<div class="alert alert-danger alert-dismissable">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
						<?php echo validation_errors(); ?>
					</div>
				<?php endif; ?>
				<form action="<?php echo base_url('auth/login') ?>" method="POST">
					<fieldset>
						<div class="form-group">
							<center><label class="col-form-label col-form-label-lg" class="align-content-center">Login</label></center>
							<input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Email" name="email">
						</div>
						<div class="form-group">
							<input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" name="password">
						</div>
						<button type="submit" class="btn btn-primary">Masuk</button>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
