
	<div class="container" align="center">
		<br><h2>Daftar Jurnal</h2><br>
		<div>
			<form action="<?php echo base_url('index.php/Journal/search') ?>" method="post" class="form-group">
				<div class="col-md-6">
					<input type="text" name="cari" placeholder="Search Your Jurnal" class="form-control">
				</div>
				<div class="col-md-8">
					<input type="submit" name="submit" class="btn btn-primary" value="Search">
				</div>
			</form>
		</div>

		<div class="row">

			<table class="table table-hover">
				<thead>
				<tr>
					<th>No</th>
					<th>Jurnal</th>
					<th>Penulis</th>
					<th>Tahun Terbit</th>
				</tr>
				</thead>
				<tbody>
				<?php foreach($listjurnal as $item){ ?>
					<tr>
						<td><?php echo $item->id; ?></td>
						<td><?php echo $item->title; ?></td>
						<td><?php echo $item->author; ?></td>
						<td><?php echo $item->year; ?></td>
						<td>
							<?php $id = $item->id;
								$file = $item->file_url;
								$lihat = base_url('index.php/journal/lihat_jurnal/');
								$download = base_url('index.php/journal/download/');
								?>
							<a class="btn btn-outline-primary" href="<?php echo $lihat.$id ?>" >Lihat Jurnal</a>
						</td>
					</tr>
					<?php } ?>
				</tbody>

			</table>


		</div>

	</div>

