
    <div class="container">
      <form class="form-horizontal" method="post" enctype="multipart/form-data" action=<?php echo base_url('journal/upload_jurnal'); ?>>

        <h2>1.Input Jurnal </h2>
        <hr>
        <div class="form-group">
          <label for="title" class="col-sm-2 control-label">Title</label>
          <div class="col-sm-10">
            <input type="text" name="title" class="form-control" id="inputTitle" placeholder="Title" autofocus required="">
          </div>
        </div>

        <div class="form-group">
          <label for="Year" class="col-sm-2 control-label">Tahun Terbit</label>
          <div class="col-sm-10">
            <input type="number" name="year" class="form-control" id="inputYear" placeholder="Year" autofocus required="">
          </div>
        </div>

        <div class="form-group">
          <label for="Page" class="col-sm-2 control-label">Jumlah Halaman</label>
          <div class="col-sm-10">
            <input type="text" name="page" class="form-control" id="inputPage" placeholder="Page" autofocus required="">
          </div>
        </div>
        <div class="form-group">
          <label for="DOI" class="col-sm-2 control-label">DOI</label>
          <div class="col-sm-10">
            <input type="text" name="DOI" class="form-control" id="inputDOI" placeholder="DOI" autofocus required="">
          </div>
        </div>

        <div class="form-group">
          <label for="JournalName" class="col-sm-2 control-label">Journal Name</label>
          <div class="col-sm-10">
            <input type="text" name="journalname" class="form-control" id="inputJournalName" placeholder="" autofocus required="">
          </div>
        </div>

        <div class="form-group">
          <label for="Journalvolume" class="col-sm-2 control-label">Journal volume</label>
          <div class="col-sm-10">
            <input type="number" volume="journalvolume" class="form-control" id="inputJournalvolume" placeholder="" autofocus required="">
          </div>
        </div>
        <br>
         <div class="form-group">
          <label for="Journalissue" class="col-sm-2 control-label">Journal issue</label>
          <div class="col-sm-10">
            <input type="number" issue="journalissue" class="form-control" id="inputJournalissue" placeholder="" autofocus required="">
          </div>
        </div>

        <h2>2. Input File</h2>
        <hr>
        <div class="form-group">
              <label class="col-sm-2 control-label">Upload Jurnal</label>
              <input type="file" name="article" id="article" required=""><br>
        </div>
        <div class="form-group">
              <div class="col-sm-3">
                    <button type="submit" name="submit" class="btn btn-success" ><span class="glyphicon glyphicon-floppy-disk"></span>Unggah</button>
              </div>
        </div>
      </form>
  </div>


