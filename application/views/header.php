<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<!-- Online -->
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/cosmo/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/cosmo/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/cosmo/_variables.scss">
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/cosmo/_bootswatch.scss">

	<!-- bootstrap 4 -->
	<link rel="stylesheet" href="<?php echo base_url('/assets/css/bootstrap.min.css'); ?>">

	<title>Form</title>
</head>
<body>

	<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
		<a class="navbar-brand" href="<?php echo base_url() ?>">Jernlib</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<div class="collapse navbar-collapse" id="navbarColor01">
			<ul class="navbar-nav mr-auto">

				<li class="nav-item active">
					<a class="nav-link" href="<?php echo base_url('index.php/Auth/index'); ?>">Home<span class="sr-only">(current)</span></a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="<?php echo base_url('index.php/Journal/index'); ?>">Daftar Jurnal</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="<?php echo base_url('index.php/Journal/index'); ?>">Kategori</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="<?php echo base_url('index.php/Journal/index'); ?>">Jurnal</a>
				</li>
			</ul>
			<form class="form-inline my-2 my-lg-0">
				<ul class="navbar-nav mr-auto">
					<?php if ($this->session->userdata('logged_in')) : ?>
						<li class="nav-item">
							<a class="nav-link" href="<?php echo base_url('auth/logout'); ?>">Logout</a>
						</li>
					<?php else : ?>
						<li class="nav-item<?php echo (uri_string() == 'login') ? ' active' : ''; ?>">
							<a class="nav-link" href="<?php echo base_url('login'); ?>">Login</a>
						</li>

						<li class="nav-item<?php echo (uri_string() == 'signup') ? ' active' : ''; ?>">
							<a class="nav-link" href="<?php echo base_url().'signup' ?>">Daftar</a>
						</li>
					<?php endif; ?>
				</ul>
			</form>
		</div>
	</nav>

