<br>
<center><h3>Upload</h3></center>

<?php echo form_open_multipart('Journal/do_upload');?>

<div class="container p-5 m-auto col-md-5 ">
	<div class="form-group text-center">
		<input name="title" type="text" class="form-control" placeholder="Title" value="<?php echo set_value('title'); ?>" required>
	</div>

	<div class="form-group text-center">
		<input name="author" type="text" class="form-control" placeholder="Author" value="<?php echo set_value('author'); ?>" required>
	</div>

	<div class="form-group text-center">
		<input name="year" type="text" class="form-control" placeholder="Year" value="<?php echo set_value('year'); ?>" required>
	</div>

	<div class="form-group text-center">
		<textarea name="doi" type="message" rows="5" cols="30" class="form-control" placeholder="Description" required></textarea>
	</div>

	<div class="form-group">
		<div style="color: red"></div>
		<div style="position:relative;">
			<a class='btn btn-primary' href='javascript:;'>
				Choose File...
				<input type="file" name="userfile" style='position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;' name="file_source" size="40"  onchange='$("#upload-file-info").html($(this).val());'>
			</a>
			&nbsp;
			<span class='label label-info' id="upload-file-info"></span>
		</div>
		<br/>
		<center><input type="submit" class="btn btn-success" value="Submit" /></center>
		<!-- <input type="file" class="" name="userfile" size="20" /> -->
	</div>
</div>


</form>

</body>
</html>
