<div>
	<div class="container" align="center">
		<br><h2>Jurnal</h2><br>

		<!-- tampilan sudah login -->
		<?php if (isset($_SESSION['logged_in'])) {?>
			<tr>
				<td>
					<div class="text-center d-md-table-row" >
						<div>
							<a href="<?php echo base_url('index.php/Journal/upload'); ?>"><img class="img-round" src="<?php echo base_url('/assets/img/download.ico'); ?>">
							</a>

						</div>
						<div>
							<a href="<?php echo base_url('index.php/Journal/upload'); ?>">
								<h5>Upload Jurnal</h5></a>
						</div>
					</div>
				</td>
				<td>
					<div class="text-center">
						<a href="<?php echo base_url('index.php/Journal/index'); ?>"><img class="img-round" src="<?php echo base_url('/assets/img/images.jpg'); ?>">
							<h5>Lihat Jurnal</h5></a>
					</div>
				</td>
			</tr>

			<!--tampilan sebelum login-->
		<?php } else{?>
			<div class="text-center mx-auto">
				<?php /*echo base_url('/assets/img/download.ico'); */?>
				<a href="<?php echo base_url('index.php/Auth/login'); ?>"><img class="img-round" src="<?php echo base_url('/assets/img/download.ico'); ?>">
					<h5>Upload</h5></a>
			</div>

			<div class="text-center mx-auto">
				<?php /*echo base_url('/assets/img/download.ico'); */?>
				<a href="<?php echo base_url('index.php/Journal/index'); ?>"><img class="img-round" src="<?php echo base_url('/assets/img/images.jpg'); ?>">
					<h5>Lihat Jurnal</h5></a>
			</div>
		<?php } ?>
	</div>
</div>
